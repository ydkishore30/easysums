//
//  EasyDocsApp.swift
//  EasyDocs
//
//  Created by dinesh yenneti on 02/10/21.
//

import SwiftUI

@main
struct EasyDocsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
